#!/bin/bash

set -e

cd tests/build
# busybox is resolved as an alias, redirect this to stdout
podman-compose build 2>&1
podman-compose up -d 2>&1
curl -isS http://localhost:8080/index.txt
podman inspect my-busybox-httpd2
podman-compose down 2>&1
